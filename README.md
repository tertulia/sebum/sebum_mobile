<div align="center">

# *Sebum* 

<a href="https://www.paypal.com/donate?hosted_button_id=82Z99Y5ZNZNT2&source=url">
  <img src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" alt="Dow com PayPal" />
</a>


#### Idealizado por [Vanessa Mota](https://gitlab.com/vanessaoliveira2706)

</div>

## Visão Geral
- Um aplicativo para facilitar e incentivar o compartilhamento de livro entre os usuários de maneira livre, bem como a fomentar a criação de clubes de leitura e a aproximação de pessoas com gostos literários semelhantes.
- Uma aplicação *Peer-to-Peer* para livros

## Por que do *Sebum*?
- Evitar a ociosidade da biblioteca pessal
- Saber quais pessoas possuem os livros desejados
- Conectar pessoas com afinidades literárias

## Tecnologias Utilizadas

- [Flutter](https://flutter.dev/docs)
- [Docker](https://www.docker.com/)
- [Dart](https://dart.dev/)
- [Slidy](https://github.com/Flutterando/slidy)
- [Modular](https://github.com/Flutterando/modular)
- [Mobx](https://github.com/mobxjs/mobx.dart)

## Pré-requisitos
 - Docker Engine
 - VS Code com as extensões [Docker](https://marketplace.visualstudio.com/items?itemName=ms-azuretools.vscode-docker) e [Remote Development](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)

## Como rodar a aplicação?

1. Clonar o projeto
```
 $ git clone https://gitlab.com/tertulia/sebum/sebum_mobile.git

# Entrar no repositório
 $ cd sebum_mobile
 ```
2. No canto inferior esquerdo clicar no ícone da extensão **Remote Development**

3. Selecionar a opção **Remote-Containers: Open Folder in Container**

4. Na janela que será aberta, selecionar o projeto **sebum_mobile** que contém o Dockerfile e clicar em abrir, o container será buildado a partir da imagem, e por fim, o terminal do container será aberto.


5. Com um **dispositivo físico** conectado rodar o comando a seguir dentro de workspace/sebum:
  ```
  $ flutter run
  ```

## API

A API deste projeto encontra-se no repositório a seguir:

https://gitlab.com/tertulia/sebum/sebum-web-api

## Contribuidores

<table>
  <tr>
    <td align="center"><a href="https://gitlab.com/vaanessamota"><img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/2824338/avatar.png?width=400" width="100px;" alt=""/><br /><sub><b>Vanessa Mota</b></sub></a><br /><sub><b>Developer & Project Lead</b></sub></a>
    </td>
    <td align="center"><a href="https://gitlab.com/israel_vitor"><img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/2248943/avatar.png?width=400" width="100px;" alt=""/><br /><sub><b>Israel Vitor</b></sub></a> 
    <br />
    <!-- colocar Jorge -->
    <sub><b>Designer</b></sub></a>
    <td align="center"><a href="https://gitlab.com/JorgeHenrys"><img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/4650458/avatar.png?width=400" width="100px;" alt=""/><br /><sub><b>Jorge Henrique</b></sub></a> 
    <br />
    <!-- colocar Jorge -->
    <sub><b>Designer</b></sub></a>
    </td>
  </tr>
</table>

