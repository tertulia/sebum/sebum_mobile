import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

class AppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sebum',
      theme: ThemeData(
        primaryColor: Color(0xffA7559F),
        accentColor: Color(0xffE5E5E5),
        backgroundColor: Color(0xffF6EDF5),
        buttonTheme: ButtonThemeData(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            buttonColor: Color(0xffA7559F)),
        fontFamily: 'Roboto',
        textTheme: TextTheme(
          caption: TextStyle(
              fontSize: 28,
              fontWeight: FontWeight.w400,
              color: Color(0xff680B5F),
              fontFamily: 'Poppins'),
          headline1: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w400,
              color: Color(0xff3d3d4c),
              fontFamily: 'Poppins'),
          headline2: TextStyle(
              fontSize: 15,
              fontWeight: FontWeight.w400,
              color: Color(0xff6c6c80)),
          bodyText1: TextStyle(
            fontSize: 15,
            height: 1.5,
          ),
        ),
      ),
      initialRoute: '/',
      navigatorKey: Modular.navigatorKey,
      onGenerateRoute: Modular.generateRoute,
    );
  }
}
