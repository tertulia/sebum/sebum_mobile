abstract class IAuthRepository {
  Future<Map> getEmailPasswordLogin(String email, String password);
}
