import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:sebum/app/shared/auth/repositories/auth_repository_interface.dart';
import 'package:sebum/app/shared/models/sharedPref.dart';
import 'package:sebum/app/shared/utils/dioError.dart';

class AuthRepository implements IAuthRepository {
  final Dio dio;
  AuthRepository(this.dio);

  @override
  Future<Map> getEmailPasswordLogin(String email, String password) async {
    SharedPref sharedPref = SharedPref();
    var responseData = {'statusCode': 0, 'isAuthenticated': false};

    try {
      Map params = {
        'email': email,
        'password': password,
      };

      var _body = json.encode(params);
      var response = await dio.post(
        'auth/login/',
        data: _body,
        options: RequestOptions(
            method: 'POST',
            contentType: Headers.jsonContentType,
            responseType: ResponseType.json,
            followRedirects: false,
            validateStatus: (status) {
              return status <= 500;
            }),
      );

      responseData['statusCode'] = response.statusCode;

      if (response.statusCode == 200 || response.statusCode == 201) {
        responseData['isAuthenticated'] = true;
        sharedPref.save(response.data["key"], "token");
        return responseData;
      } else if (response.statusCode == 400) {
        throw Exception("Email ou senha incorretos");
      } else
        throw Exception('Erro na autenticação');
    } catch (exception) {
      if (exception.runtimeType == 'DioError') {
        final ErrorMessage = DioExceptions.fromDioError(exception).toString();
        print(ErrorMessage);
      }
    }
    return responseData;
  }
}
