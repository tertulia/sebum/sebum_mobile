import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:sebum/app/shared/auth/repositories/auth_repository.dart';
import 'package:sebum/app/shared/models/sharedPref.dart';
part 'auth_controller.g.dart';

class AuthController = _AuthControllerBase with _$AuthController;

abstract class _AuthControllerBase with Store {
  final AuthRepository _authRepository = Modular.get();

  @observable
  bool isAuthenticated = false;

  @observable
  int statusCode;

  SharedPref sharedPref = SharedPref();

  void loginWithPassword(String email, String password) async {
    var response = await _authRepository.getEmailPasswordLogin(email, password);
    isAuthenticated = response.values.last;
    statusCode = response.values.first;
  }

  void logOut() async {
    await sharedPref.clear();
  }
}
