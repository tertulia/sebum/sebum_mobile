import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextInputs extends StatelessWidget {
  final String hintText;
  final TextInputType textInputType;
  final Function onChange;
  final bool obscure;
  final void Function(String) validation;

  TextInputs(
      {this.hintText,
      this.textInputType,
      this.onChange,
      this.obscure,
      this.validation});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      maxLines: 1,
      keyboardType: textInputType,
      obscureText: obscure,
      style: TextStyle(fontWeight: FontWeight.bold),
      decoration: new InputDecoration(
          hintText: hintText,
          filled: true,
          fillColor: Colors.grey[100],
          hintStyle: TextStyle(color: Colors.grey),
          errorStyle: TextStyle(fontSize: 16),
          border: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(15.0)),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: const BorderRadius.all(Radius.circular(15.0)),
            borderSide: const BorderSide(color: Color(0xfff5f5f5), width: 0.0),
          )),
      validator: validation,
      onChanged: onChange,
    );
  }
}
