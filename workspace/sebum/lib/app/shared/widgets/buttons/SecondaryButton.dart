import 'package:flutter/material.dart';

class SecondaryButton extends StatelessWidget {
  final String buttonText;
  final Function action;
  final Size size;

  SecondaryButton({this.buttonText, this.action, this.size});

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        shape: Theme.of(context).buttonTheme.shape,
        primary: Color(0xffF6EDF5),
        // fixedSize: Size(200, 50),
        minimumSize: size,
        side: BorderSide(
          color: Theme.of(context).buttonColor,
          width: 1,
        ),
      ),
      child: new Text(
        buttonText,
        style: new TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor),
      ),
      onPressed: () {
        action();
      },
    );
  }
}
