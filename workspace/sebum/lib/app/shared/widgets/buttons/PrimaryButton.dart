import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget  {
final String buttonText;
final Function action;
final Size size;

PrimaryButton({this.buttonText, this.action, this.size});

@override
  Widget build(BuildContext context) {
    return ElevatedButton(
          style: ElevatedButton.styleFrom(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
            primary: Theme.of(context).primaryColor,
            // fixedSize: Size(200, 50),
            minimumSize: size,
            ),
          child: new Text(buttonText,
            style: new TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold, color: Colors.white),
          ),
          onPressed: (){
            action();
          },
          );
  }
}