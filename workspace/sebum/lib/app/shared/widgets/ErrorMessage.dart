import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:sebum/app/modules/login/login_controller.dart';

class ErrorMessage extends StatelessWidget {
  const ErrorMessage({
    Key key,
    @required this.controller,
  }) : super(key: key);

  final LoginController controller;

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      if (controller.errorMessage.length > 0 &&
          controller.errorMessage != null) {
        return Padding(
          padding: const EdgeInsets.fromLTRB(10.0, 20.0, 0.0, 0.0),
          child: Text(
            controller.errorMessage,
            style: TextStyle(
                fontSize: 18.0,
                color: Colors.red[700],
                height: 1.0,
                fontWeight: FontWeight.normal),
          ),
        );
      } else {
        return new Container(height: 0.0);
      }
    });
  }
}
