import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:sebum/app/modules/login/login_controller.dart';
import 'package:sebum/app/shared/widgets/ErrorMessage.dart';
import 'package:sebum/app/shared/widgets/TextInputs.dart';
import 'package:sebum/app/shared/widgets/buttons/PrimaryButton.dart';
import 'package:sebum/app/shared/widgets/buttons/SecondaryButton.dart';
import 'package:sebum/app/shared/widgets/Logo.dart';

class LoginPage extends StatefulWidget {
  final String title;
  const LoginPage({Key key, this.title = "Login"}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends ModularState<LoginPage, LoginController> {
  final _formKey = new GlobalKey<FormState>();

  bool _submitForm() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();

      controller.loginWithPassword();
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: null,
      body: Observer(
        builder: (_) => controller.loading == true
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView(
                children: <Widget>[
                  controller.loading
                      ? Center(child: CircularProgressIndicator())
                      : _showBody(screenSize.height)
                ],
              ),
      ),
    );
  }

  Widget _showBody(screenHeight) {
    var paddingTopRatio = 0.09;
    screenHeight < 600 ? paddingTopRatio = 0.009 : paddingTopRatio;

    return new Container(
      padding: EdgeInsets.all(20.0),
      child: new Form(
        key: _formKey,
        child: new ListView(
          shrinkWrap: true,
          children: <Widget>[
            SizedBox(height: screenHeight * paddingTopRatio),
            Logo(),
            _showAppTitleText(),
            SizedBox(height: screenHeight * 0.06),
            _showSignText(),
            SizedBox(height: screenHeight * 0.03),
            TextInputs(
              hintText: 'Email',
              onChange: controller.changeEmail,
              textInputType: TextInputType.emailAddress,
              validation: (value) {
                if (value.isEmpty) {
                  return 'Preencha seu e-mail';
                }
                return null;
              },
              obscure: false,
            ),
            SizedBox(height: screenHeight * 0.03),
            TextInputs(
                hintText: 'Senha',
                onChange: controller.changePassword,
                validation: (value) {
                  if (value.isEmpty) {
                    return 'Preencha sua senha';
                  }
                  return null;
                },
                obscure: true),
            Observer(
              builder: (_) => ErrorMessage(controller: controller),
            ),
            SizedBox(height: 20),
            PrimaryButton(
              buttonText: 'Entrar',
              action: _submitForm,
              size: Size(335, 56),
            ),
            SizedBox(height: 20),
            _showHaveAnAccountText(),
            SizedBox(height: 5),
            SecondaryButton(
              buttonText: 'Cadastrar',
              size: Size(335, 56),
            )
          ],
        ),
      ),
    );
  }

  Widget _showAppTitleText() {
    return Center(
        child: Text('SEBUM', style: Theme.of(context).textTheme.caption));
  }

  Widget _showSignText() {
    return Text(
      'Entrar',
      style: TextStyle(
          fontSize: 20, fontWeight: FontWeight.bold, color: Color(0xff5A5A5A)),
    );
  }

  Widget _showHaveAnAccountText() {
    return Center(
        child: Text(
      'Não possui conta?',
      style: TextStyle(fontSize: 20, color: Color(0xff212121)),
    ));
  }
}
