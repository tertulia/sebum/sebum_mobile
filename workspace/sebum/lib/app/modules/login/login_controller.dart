import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:mobx/mobx.dart';
import 'package:sebum/app/shared/auth/controllers/auth_controller.dart';

part 'login_controller.g.dart';

class LoginController = _LoginBase with _$LoginController;

abstract class _LoginBase with Store {
  AuthController auth = AuthController();

  @observable
  bool loading = false;

  @observable
  String errorMessage = "";

  @action
  updateErrorMessage(String value) => errorMessage = value;

  @observable
  String email;

  @action
  changeEmail(String value) => email = value;

  @observable
  String password;

  @action
  changePassword(String value) => password = value;

  @action
  Future loginWithPassword() async {
    try {
      loading = true;
      await auth.loginWithPassword(email, password);

      if (auth.isAuthenticated != false) {
        Modular.to.pushReplacementNamed('/home');
      } else {
        loading = false;
        auth.statusCode == 400
            ? updateErrorMessage('Email ou senha incorretos!')
            : updateErrorMessage('Erro na autenticação!');
      }
    } on Exception catch (error) {
      print(error.toString().substring(11));
      updateErrorMessage(error.toString().substring(11));
    }
  }

  @action
  Future logOut() async {
    auth.logOut();
    Modular.to.pushNamedAndRemoveUntil('/', ModalRoute.withName('/'));
  }
}
