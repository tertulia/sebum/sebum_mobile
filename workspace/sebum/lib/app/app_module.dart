import 'package:dio/dio.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter/material.dart';
import 'package:sebum/app/app_widget.dart';
import 'package:sebum/app/modules/home/home_module.dart';
import 'package:sebum/app/modules/login/login_module.dart';
import 'package:sebum/app/shared/auth/repositories/auth_repository.dart';
import 'package:sebum/app/shared/auth/repositories/auth_repository_interface.dart';
import 'package:sebum/app/shared/utils/constants.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => Dio(BaseOptions(baseUrl: URL_BASE))),
        Bind<IAuthRepository>((i) => AuthRepository(i.get<Dio>())),
        Bind((i) => LoginModule()),
        Bind((i) => HomeModule()),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter('/', module: LoginModule()),
        ModularRouter('/home', module: HomeModule()),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
