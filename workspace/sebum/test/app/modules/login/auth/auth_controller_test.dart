import 'package:flutter_modular/flutter_modular.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:sebum/app/app_module.dart';
import 'package:sebum/app/modules/login/login_module.dart';
// import 'package:sebum/app/shared/auth/auth_controller.dart';
import 'package:sebum/app/shared/auth/repositories/auth_repository.dart';
// import 'package:sebum/app/shared/models/user.dart';

class AuthRepositoryMock extends Mock implements AuthRepository {}

void main() {
  // AuthRepositoryMock _authRepository;
  // AuthController authController;

  setUp(() {
    initModule(AppModule(), changeBinds: [
      Bind((i) => AuthRepositoryMock()),
    ]);
    initModule(LoginModule());

    // authController = LoginModule.to.get<AuthController>();
    // _authRepository = Modular.get<AuthRepositoryMock>();
  });

  //  test('should return a user when loginWithPassword is called', ()  async {

  //     final expectedUser = User(name: 'User', numOfBooks: 0, telegram: null, bio: null, createdAt: "2021-03-26T17:33:00.594332", email: "user@sebum.com", evaluation: 0.0, image: null, lastLogin: null, token: "9377c24e91cfd34140d1280e08e6223f3addf953");

  //     when(_authRepository.getEmailPasswordLogin('user@sebum.com', '123456')).thenAnswer((_) async => expectedUser);

  //     final returnedUser = await authController.loginWithPassword('user@sebum.com', '123456');

  //     expect(returnedUser, equals(expectedUser));

  // });
}
