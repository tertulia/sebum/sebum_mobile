import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_modular/flutter_modular_test.dart';
// import 'package:sebum/app/modules/home/home_controller.dart';
import 'package:sebum/app/modules/home/home_module.dart';

import 'package:sebum/app/modules/home/home_page.dart';

void main() {
  initModule(HomeModule());
  // HomeController home;

  setUp(() {
    // home = HomeModule.to.get<HomeController>();
  });
  testWidgets('HomePage has title', (tester) async {
    await tester.pumpWidget(buildTestableWidget(HomePage(title: 'Home')));
    final titleFinder = find.text('Home');
    expect(titleFinder, findsOneWidget);
  });
}
