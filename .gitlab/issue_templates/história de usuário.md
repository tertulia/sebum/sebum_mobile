## Hisória de usuário

### Descriçao da História de usuário

- **COMO UM(A)** ( tipo de usuário ) **EU QUERO** ( realizer alguma tarefa ) **PARA QUE EU POSSA** ( alcançar um objetivo ).

### Tasks
- [ ] (<nome da task>)[#<numero da task>]
- [ ] (<nome da task>)[#<numero da task>]


/label ~"história::<numero da história>" 

/due < in 2 days | this friday | december 31st >
/weight < 1 2 3 >
/assign @
/milestone %"<nome da milestone>"